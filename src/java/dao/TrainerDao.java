package dao;

import entities.Trainer;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class TrainerDao {

    public Collection<Trainer> getAllTrainers() {
        Collection<Trainer> trainers = null;
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("bc8jpa2PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("SELECT e FROM Trainer e");
            trainers = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
            emf.close();
        }
        return trainers;
    }

    public Trainer findTrainerById(Long id) {
        Trainer t = null;
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("bc8jpa2PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            t = em.find(entities.Trainer.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
            emf.close();
        }
        return t;
    }

    public boolean deleteTrainerById(Long id) {
        boolean deleteOK = false;
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("bc8jpa2PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Trainer temp = em.find(entities.Trainer.class, id);
            em.remove(temp);

            em.getTransaction().commit();
            deleteOK = true;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
            emf.close();
        }
        return deleteOK;
    }

    public boolean insertTrainer(Trainer t) {
        boolean insertOK = false;
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("bc8jpa2PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(t);
            em.getTransaction().commit();
            insertOK = true;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
            emf.close();
        }
        return insertOK;
    }

    public boolean updateTrainer(Trainer t) {
        boolean insertOK = false;
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("bc8jpa2PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Trainer temp = em.find(entities.Trainer.class, t.getId());
            temp.setFirstName(t.getFirstName());
            temp.setLastName(t.getLastName());
            temp.setSubject(t.getSubject());
            em.getTransaction().commit();
            insertOK = true;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
            emf.close();
        }
        return insertOK;
    }
}
